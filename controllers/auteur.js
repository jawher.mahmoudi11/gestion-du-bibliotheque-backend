const Auteur = require('../models/auteur');


exports.getAllAuteur = (req, res) => {
    Auteur.find()
        .then(auteurs => res.json(auteurs))
        .catch(error => res.json({ error }));
};

exports.getAuteur = (req, res) => {
    Auteur.findOne({ _id: req.params.id })
        .then(auteur => res.json(auteur))
        .catch(error => res.json({ error }));
};

exports.createAuteur = (req, res) => {

        const auteur = new Auteur({
            nom: req.body.nom
        });
        auteur.save()
          .then(() => res.status(201).json({ message: 'Auteur créé !' }))
          .catch(error => res.status(400).json({ error }));
      
  };

exports.updateAuteur = (req, res) => {
    Auteur.updateOne({ _id: req.params.id }, {
        nom: req.body.nom
    })
        .then(() => res.json({ message: 'auteur updated !' }))
        .catch(error => res.json({ error }));
};


exports.deleteAuteur = (req, res) => {
    Auteur.deleteOne({ _id: req.params.id })
        .then(() => res.json({ message: 'auteur deleted !' }))
        .catch(error => res.json({ error }));
};
