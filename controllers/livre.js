const Livre = require('../models/livre');


exports.getAllLivre = (req, res) => {
    Livre.find().populate('idAuteur').populate('idType')
        .then(livres => res.json(livres))
        .catch(error => res.json({ error }));
};

exports.getLivre = (req, res) => {
    Livre.findOne({ _id: req.params.id })
        .then(livre => res.json(livre))
        .catch(error => res.json({ error }));
};

exports.createLivre = (req, res) => {

        const livre = new Livre({
            titre: req.body.titre,
            maisonEdition:req.body.maisonEdition,
            dateSortie:req.body.dateSortie,
            idAuteur:req.body.idAuteur,
            idType:req.body.idType
        });
        livre.save()
          .then(() => res.status(201).json({ message: 'Livre créé !' }))
          .catch(error => res.status(400).json({ error }));
      
  };

exports.updateLivre = (req, res) => {
    Livre.updateOne({ _id: req.params.id }, {
        titre: req.body.titre,
        maisonEdition:req.body.maisonEdition,
        dateSortie:req.body.dateSortie,
        idAuteur:req.body.idAuteur
        })
        .then(() => res.json({ message: 'livre updated !' }))
        .catch(error => res.json({ error }));
};


exports.deleteLivre = (req, res) => {
    Livre.deleteOne({ _id: req.params.id })
        .then(() => res.json({ message: 'livre deleted !' }))
        .catch(error => res.json({ error }));
};
