const TypeLivre = require('../models/type');


exports.getAllTypeLivre = (req, res) => {
    TypeLivre.find()
        .then(typeLivres => res.json(typeLivres))
        .catch(error => res.json({ error }));
};

exports.getTypeLivre = (req, res) => {
    TypeLivre.findOne({ _id: req.params.id })
        .then(typeLivre => res.json(typeLivre))
        .catch(error => res.json({ error }));
};

exports.createTypeLivre = (req, res) => {

        const typeLivre = new TypeLivre({
            typeTitre: req.body.typeTitre
        });
        typeLivre.save()
          .then(() => res.status(201).json({ message: 'TypeLivre créé !' }))
          .catch(error => res.status(400).json({ error }));
      
  };

exports.updateTypeLivre = (req, res) => {
    TypeLivre.updateOne({ _id: req.params.id }, {
        typeTitre: req.body.typeTitre
    })
        .then(() => res.json({ message: 'typeLivre updated !' }))
        .catch(error => res.json({ error }));
};


exports.deleteTypeLivre = (req, res) => {
    TypeLivre.deleteOne({ _id: req.params.id })
        .then(() => res.json({ message: 'typeLivre deleted !' }))
        .catch(error => res.json({ error }));
};
