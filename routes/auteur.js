const express = require('express');
const router = express.Router();
const contrAuteur = require('../controllers/auteur');
const auth = require('../middleware/auth');

router.get('/', contrAuteur.getAllAuteur);
router.get('/:id', auth, contrAuteur.getAuteur);
router.post('/', auth, contrAuteur.createAuteur);
router.put('/:id', auth, contrAuteur.updateAuteur);
router.delete('/:id', auth, contrAuteur.deleteAuteur);


module.exports = router