const express = require('express');
const router = express.Router();
const contrTypeLivre = require('../controllers/type');
const auth = require('../middleware/auth');

router.get('/', contrTypeLivre.getAllTypeLivre);
router.get('/:id', contrTypeLivre.getTypeLivre);
router.post('/', contrTypeLivre.createTypeLivre);
router.put('/:id', contrTypeLivre.updateTypeLivre);
router.delete('/:id', contrTypeLivre.deleteTypeLivre);


module.exports = router