const express = require('express');
const router = express.Router();
const contrUser = require('../controllers/user');
const auth = require('../middleware/auth');
const authRole = require('../middleware/authRole');

router.get('/', contrUser.getAllUser);
router.get('/:id', authRole, contrUser.getUser);
router.post('/', contrUser.createUser);
router.put('/:id', auth, contrUser.updateUser);
router.delete('/:id', auth, contrUser.deleteUser);


module.exports = router