const express = require('express');
const router = express.Router();
const contrLivre = require('../controllers/livre');
const auth = require('../middleware/auth');

router.get('/', contrLivre.getAllLivre);
router.get('/:id', contrLivre.getLivre);
router.post('/',  contrLivre.createLivre);
router.put('/:id', contrLivre.updateLivre);
router.delete('/:id', contrLivre.deleteLivre);


module.exports = router