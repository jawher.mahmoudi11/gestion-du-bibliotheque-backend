const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  nom: { type: String , required: true  },
  // email: { type: String,unique : true },
  email: {
    type: String,
    required: true,
    default: '',
    unique: true,
    validate:
    {
        validator: function ValidateEmail(v) {
            return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v))
            //w{2,3} lazemha tkoun fila5er .org .com .tn ya3ni . w ba3édha 7aja tetkawén par 2 ou 3 caractéresZZZ
        },
        message: 'email is invalid'
    }
},
  role: { type: String},
  mot_de_passe: { type: String , required: true  },
},{ timestamps: true });

module.exports = mongoose.model('User', userSchema);