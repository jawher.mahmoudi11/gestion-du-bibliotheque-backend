const mongoose = require('mongoose');

const livreSchema = mongoose.Schema({
  titre: { type: String ,unique : true },
  maisonEdition: { type: String },
  dateSortie: { type: Date },
  idAuteur: {
    type: mongoose.Schema.Types.ObjectId,
   //required: REQUIRED_VALIDATION_MESSAGE,
   ref: 'Auteur'
  },
  idType: {
    type: mongoose.Schema.Types.ObjectId,
   //required: REQUIRED_VALIDATION_MESSAGE,
   ref: 'TypeLivre'
  },

},{ timestamps: true });

module.exports = mongoose.model('Livre', livreSchema);