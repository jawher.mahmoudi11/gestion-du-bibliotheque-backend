const mongoose = require('mongoose');

const auteurSchema = mongoose.Schema({
  nom: { type: String ,unique : true , required: true  }
},{ timestamps: true });

module.exports = mongoose.model('Auteur', auteurSchema);