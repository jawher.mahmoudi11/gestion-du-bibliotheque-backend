const mongoose = require('mongoose');

const typeLivreSchema = mongoose.Schema({
  typeTitre: { type: String ,unique : true , required: true }
},{ timestamps: true });

module.exports = mongoose.model('TypeLivre', typeLivreSchema);