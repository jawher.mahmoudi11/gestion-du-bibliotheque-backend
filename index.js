const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const userRouter = require('./routes/user');
const livreRouter = require('./routes/livre');
const auteurRouter = require('./routes/auteur');
const typeRouter = require('./routes/type');
const contrUser = require('./controllers/user');
var http = require('http').Server(app);

var cors =  require('cors')

const port = process.env.PORT || 3000;

mongoose.connect('mongodb://localhost:27017/bibliotheque2', {useNewUrlParser: true,useUnifiedTopology: true, useCreateIndex: true});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true }));
app.use(cors());
app.post('/api/login', contrUser.login);
app.use('/api/user', userRouter);
app.use('/api/type', typeRouter);
app.use('/api/auteur', auteurRouter);
app.use('/api/livre', livreRouter);



http.listen(port, () => {
    console.log('Server app listening on port ' + port);
});